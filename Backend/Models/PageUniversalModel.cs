﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Text;

namespace BP_AttendanceSystem_Lepine.Models {
    public class PageUniversalModel<T> {
        public PageUniversalModel() {
            Message = null;
            Color = "";
        }
        public PageUniversalModel(T model, string? message = null, string color = "alert-secondary") {
            Data = model;
            Color = color;
            Message = message;
        }
        public string? Message { get; set; }
        public string? Color { get; set; }
        public T Data { get; set; }
        public bool HasMessage() {
            return Message != null;
        }
        public string GetStringFromValidation(ModelStateDictionary.ValueEnumerable keyValues) {
            StringBuilder sb = new StringBuilder();
            foreach (var key in keyValues) {
                sb.Append(key.AttemptedValue);
                sb.Append(": ");
                sb.AppendLine(key.ValidationState.ToString());
            }
            return sb.ToString();
        }
    }
}
