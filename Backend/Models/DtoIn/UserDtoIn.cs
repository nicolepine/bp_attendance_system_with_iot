﻿namespace BP_AttendanceSystem_Lepine.Models.DtoIn {
    public class UserDtoIn {
        public class Login {
            public string Email { get; set; }
            public string Password { get; set; }
        }
    }
}
