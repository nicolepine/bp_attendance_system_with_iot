﻿namespace BP_AttendanceSystem_Lepine.Models.DtoIn {
    public class DeviceDtoIn {
        public enum AccessType {
            Arrival = 0,
            Departure = 1
               }
        public class Auth {
            public string? Uid { get; set; }
            public string? Manufacturer { get; set; }
        }
        public class DeviceToken {
            public string? Token { get; set; }
        }
        public class Scan : DeviceToken {
            public AccessType? AccessType { get; set; }
            public DateTime? ScanTime { get; set; }
            public string? ChipId { get; set; }
            public string? FingerprintData { get; set; }

            public bool IsValid() {
                return AccessType != null && ScanTime != null && (ChipId != null || FingerprintData != null);
            }
        }
    }
}
