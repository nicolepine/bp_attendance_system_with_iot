﻿using BP_AttendanceSystem_Lepine.Models;
using BP_AttendanceSystem_Lepine.Models.Db;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Security.Claims;

namespace BP_AttendanceSystem_Lepine.Controllers {
    [Authorize]
    public class DashboardController : Controller {
        private readonly ILogger<DashboardController> _logger;
        private GlobalContext _globalContext;

        public DashboardController(ILogger<DashboardController> logger, GlobalContext gc) {
            _logger = logger;
            _globalContext = gc;
        }

        public IActionResult Index() {
            return RedirectToAction("MyShifts");
        }
        [HttpGet]
        [Route("MyShifts")]
        public async Task<IActionResult> MyShifts(DateTime month = default) {
            if(month == default) {
                month = DateTime.Now;
            }
            DateTime from = new(month.Year, month.Month, 1);
            DateTime to = from.AddMonths(1).AddDays(-1);
            ViewBag.Month = month;
            int id = int.Parse(User.Claims.FirstOrDefault(u => u.Type == ClaimTypes.Sid).Value);
            IEnumerable<Shift> shifts = await _globalContext.Shifts
                .Include(s => s.User)
                .Include(s => s.ArrivalDevice)
                .Include(s => s.DepartureDevice)
                .Where(s => s.UserId == id
            && ((s.Arrival.Value.Date >= from.Date && s.Departure == null && s.Arrival.Value.Date <= to.Date)
            || (s.Arrival.Value.Date >= from.Date && s.Departure.Value.Date <= to.Date)
            || (s.Departure.Value.Date <= to.Date && s.Arrival == null && s.Departure.Value.Date >= from.Date)))
                .ToListAsync();

            return View(new PageUniversalModel<IEnumerable<Shift>>(shifts));
        }

        [HttpGet]
        [Route("Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}