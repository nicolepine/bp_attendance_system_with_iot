﻿using BP_AttendanceSystem_Lepine.Models;
using BP_AttendanceSystem_Lepine.Models.Db;
using BP_AttendanceSystem_Lepine.Models.DtoIn;
using BP_AttendanceSystem_Lepine.Models.DtoOut;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static BP_AttendanceSystem_Lepine.Models.DtoIn.DeviceDtoIn;

namespace BP_AttendanceSystem_Lepine.Controllers {


    [Route("/api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase {

        private static Dictionary<string, Device> _authDevices = new();
        private GlobalContext _globalContext;
        private readonly ILogger<DeviceController> _logger;

        public DeviceController(ILogger<DeviceController> logger, GlobalContext gc) {
            _globalContext = gc;
            _logger = logger;
        }

        [HttpPost]
        [Route("Auth")]
        public async Task<IActionResult> Auth(DeviceDtoIn.Auth authData) {
            Device? device = await _globalContext.Devices.SingleOrDefaultAsync(d => d.Uid == authData.Uid);
            if (device == default) {
                _logger.LogWarning($"An unknown device with uid {authData.Uid}, tried to log in.");
                return Unauthorized(new ApiDtoOut.Response("This device cannot be authorized.", 401));
            }
            string token = Guid.NewGuid().ToString();
            _authDevices.Add(token, device);
            ApiDtoOut.TokenDtoOut result = new() {
                Token = token
            };
            return Ok(result);
        }

        [HttpPost]
        [Route("OnScan")]
        public async Task<IActionResult> OnScan(DeviceDtoIn.Scan scan) {
            if(IsDeviceAuthorized(scan.Token)) {
                if(!scan.IsValid()) {
                    return BadRequest(new ApiDtoOut.Response("Nesprávný požadavek", 400));
                }
                bool isNew = true;
                //Check table for chipId or fingerprint data
                AccessDevice? accessDevice = await GetAccessDevice(scan);
                if (accessDevice == default) {
                    return BadRequest(new ApiDtoOut.Authorization("Neznámý čip", 400) { AllowEntry = ApiDtoOut.Entry.Blocked });
                }
                Shift? shift = new();
                DateTime convertedDate = DateTime.SpecifyKind(scan.ScanTime.Value, DateTimeKind.Utc);
                if (scan.AccessType == DeviceDtoIn.AccessType.Arrival) {
                    shift.Arrival = convertedDate;
                    shift.UserId = accessDevice.UserId;
                    shift.SensorArrivalId = _authDevices[scan.Token].Id;
                }
                if(scan.AccessType == DeviceDtoIn.AccessType.Departure) {
                    shift = await _globalContext.Shifts.OrderByDescending(s => s.Arrival).FirstOrDefaultAsync(s => s.UserId == accessDevice.UserId && s.Arrival <= scan.ScanTime && s.Departure == null);
                    if (shift == default) {
                        shift = new();
                        shift.UserId = accessDevice.UserId;
                    } else {
                        isNew = false;
                    }
                    shift.Departure = convertedDate;
                    shift.SensorDepartureId = _authDevices[scan.Token].Id;
                }
                if (isNew) {
                    _globalContext.Shifts.Add(shift);
                } else {
                    _globalContext.Shifts.Update(shift);
                }
                await _globalContext.SaveChangesAsync();
                return Ok(new ApiDtoOut.Authorization("Vstup povolen") { AllowEntry = ApiDtoOut.Entry.Allow});
            }
            return Unauthorized(new ApiDtoOut.Authorization("Napřihlášen", 401) { AllowEntry = ApiDtoOut.Entry.Blocked });
        }

        [HttpPost]
        [Route("CheckEntry")]
        public IActionResult CheckEntry() {

            return Ok();
        }

        [HttpGet]
        [Route("GetUser")]
        public async Task<IActionResult> GetUser(DeviceDtoIn.Scan scan) {
            if (IsDeviceAuthorized(scan.Token)) {
                int? id = _authDevices[scan.Token].Id;
                AccessDevice? accessDevice = await GetAccessDevice(scan);
                if (accessDevice != default) {
                    return Ok(new ApiDtoOut.UserInfo("Úspěch") { FullName = accessDevice.User.GetFullName() });
                } else {
                    return BadRequest(new ApiDtoOut.Response("Neznámý čip", 400));
                }
            }
            return Unauthorized();
        }

        [HttpDelete]
        [Route("Logout")]
        public IActionResult Logout(string? token) {
            if (IsDeviceAuthorized(token)) {
                _authDevices.Remove(token);
                return Ok(new ApiDtoOut.Response("Odhlášeno"));
            }
            return BadRequest();
        }
        #region Helpers
        private bool IsDeviceAuthorized(string? token) {
            if(token == null) {
                return false;
            }
            return _authDevices.Any(p => p.Key == token);
        }
        private async Task<AccessDevice?> GetAccessDevice(Scan scan) {
            AccessDevice? accessDevice;
            IQueryable<AccessDevice> call = _globalContext.AccessDevices.Include(ac => ac.User);
            if (scan.ChipId != null) {
                accessDevice = await call.SingleOrDefaultAsync(ad => ad.ChipId == scan.ChipId);
            } else if (scan.FingerprintData != null) {
                accessDevice = await call.SingleOrDefaultAsync(ad => ad.FingerprintData == scan.FingerprintData);
            } else {
                return default;
            }
            return accessDevice;
        }
        #endregion
    }
}
