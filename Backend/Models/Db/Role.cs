﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BP_AttendanceSystem_Lepine.Models.Db {
    [Table("roles")]
    public class Role {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("role_name")]
        public string? RoleName { get; set;}

        public virtual ICollection<User> Users { get; set; } 
    }
}
