﻿using Microsoft.AspNetCore.Authorization;
using System.Security.Cryptography;
using System.Text;

namespace BP_AttendanceSystem_Lepine.Helpers {
    public class AppHelper {
        private static List<int> _adminIdList;
        public static void SetAdminRoleList(string roleList) {
            _adminIdList = new List<int>();
            foreach (string id in roleList.Split(",")) {
                _adminIdList.Add(int.Parse(id));
            }
        }
        public static List<int> GetAdminIdRoleList() {
            return _adminIdList;
        }
        public static string ComputeSha256Hash(string rawPassword) {
            using (SHA256 sha256Hash = SHA256.Create()) {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawPassword));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++) {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
    public class AdminAuthorizeAttribute : AuthorizeAttribute {
        public AdminAuthorizeAttribute() {
            this.Roles = System.Configuration.ConfigurationManager.AppSettings["AdminIdRoleList"];
        }
    }
}
