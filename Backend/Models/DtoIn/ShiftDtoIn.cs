﻿namespace BP_AttendanceSystem_Lepine.Models.DtoIn {
    public class ShiftDtoIn {
        public class TimeUpdate {
            public int Id { get; set; }
            public string? Type { get; set; }
            public TimeSpan? Time { get; set; }
        }
    }
}
