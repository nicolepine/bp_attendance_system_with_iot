﻿using BP_AttendanceSystem_Lepine.Models.Db;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using BP_AttendanceSystem_Lepine.Models;
using BP_AttendanceSystem_Lepine.Models.DtoIn;
using BP_AttendanceSystem_Lepine.Helpers;

namespace BP_AttendanceSystem_Lepine.Controllers {
    [Route("/Auth")]
    public class AuthController : Controller {
        private readonly ILogger<AuthController> _logger;
        private GlobalContext _globalContext;

        public AuthController(ILogger<AuthController> logger, GlobalContext gc) {
            _logger = logger;
            _globalContext = gc;
        }
        public IActionResult Index() {
            return RedirectToAction("Login");
        }
        [HttpGet]
        [Route("Login")]
        public IActionResult Login() {
            ViewBag.Error = HttpContext.GetRouteValue("Error");
            return View();
        }
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(UserDtoIn.Login loginInfo) {

            // V prototypu jen jednoduchá kontrola na BE
            if(string.IsNullOrEmpty(loginInfo.Password) || string.IsNullOrEmpty(loginInfo.Email)) {
                return RedirectToAction("Login", new { Error = "Pole nemohou být prázdná." });
            }

            User? verifiedUser = _globalContext.Users.SingleOrDefault(u => u.Email == loginInfo.Email && u.PasswordHash == AppHelper.ComputeSha256Hash(loginInfo.Password));

            if (verifiedUser == default) {
                return RedirectToAction("Login", new { Error = "Špatné údaje." });
            }

            var claims = new List<Claim> {
                new Claim(ClaimTypes.Email, verifiedUser.Email),
                new Claim(ClaimTypes.Name, verifiedUser.FirstName),
                new Claim(ClaimTypes.Sid, verifiedUser.Id.ToString()),
                new Claim(ClaimTypes.Role, verifiedUser.RoleId.ToString())
            };


            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);
            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity));
            _logger.LogInformation($"{verifiedUser.Email} logged in.");
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<IActionResult> Logout() {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}
