﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BP_AttendanceSystem_Lepine.Models.Db {
    [Table("devices")]
    public class Device {
        [Key]
        [Column("id")]
        public int? Id { get; set; }
        [Column("device_name")]
        [Display(Name = "Název zařízení")]
        public string? DeviceName { get; set; }
        [Column("description")]
        [Display(Name = "Popis")]

        public string? Description { get; set; }
        [Column("uid")]
        public string? Uid { get; set; }

        //public virtual ICollection<Shift>? Shifts { get; set; }
    }
}
