﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BP_AttendanceSystem_Lepine.Models.Db {
    [Table("shifts")]
    public class Shift {
        [Key]
        [Column("id")]
        public int? Id { get; set; }
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("arrival")]
        [Display(Name = "Příchod")]
        public DateTime? Arrival { get; set; }
        [Column("departure")]
        [Display(Name = "Odchod")]
        public DateTime? Departure { get; set; }
        [Column("sensor_arrival_id")]
        [ForeignKey("ArrivalDevice")]
        public int? SensorArrivalId { get; set; }
        [Column("sensor_departure_id")]
        [ForeignKey("DepartureDevice")]
        public int? SensorDepartureId { get; set; }

        [Display(Name = "Zaměstnanec")]
        [ForeignKey("UserId")]
        public virtual User? User { get; set; }
        [Display(Name = "Zařízení pro přístup")]
        public virtual Device? ArrivalDevice { get; set; }
        [Display(Name = "Zařízení pro odchod")]
        public virtual Device? DepartureDevice { get; set; }

        public string GetDate() {
            if (Arrival != null) {
                return Arrival.Value.ToLocalTime().ToShortDateString();
            } else if (Departure != null) {
                return Departure.Value.ToLocalTime().ToShortDateString();
            }
            return "";
        }

        public string GetArrivalTime() {
            return Arrival == null ? "--:--" : Arrival.Value.ToLocalTime().ToString("HH:mm");
        }
        public string GetDepartureTime() {
            return Departure == null ? "--:--" : Departure.Value.ToLocalTime().ToString("HH:mm");
        }
    }
}
