#CREATE DATABASE bp_dochazka

USE bp_dochazka;

CREATE TABLE roles (
id INT NOT NULL AUTO_INCREMENT,
role_name VARCHAR(20),
PRIMARY KEY(id)
);

CREATE TABLE users (
id INT NOT NULL AUTO_INCREMENT,
first_name VARCHAR(40),
surname VARCHAR(40),
phone VARCHAR(20),
email VARCHAR(60),
password_hash VARCHAR(120),
role_id INT NOT NULL,
PRIMARY KEY(id),
CONSTRAINT FK_Role_User FOREIGN KEY(role_id) REFERENCES roles(id)
);


CREATE TABLE devices (
id INT NOT NULL AUTO_INCREMENT,
device_name VARCHAR(50),
"description" VARCHAR(100),
uid VARCHAR(30),
PRIMARY KEY(id)
);

CREATE TABLE access_devices (
id INT NOT NULL AUTO_INCREMENT,
user_id INT,
chip_id VARCHAR(200),
"description" VARCHAR(100),
fingerprint_data VARCHAR(500),
PRIMARY KEY(id),
CONSTRAINT FK_AccessDevice_User FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE shifts (
id INT NOT NULL AUTO_INCREMENT,
user_id INT NOT NULL,
arrival DATETIME NULL,
departure DATETIME NULL,
sensor_arrival_id INT NULL,
sensor_departure_id INT NULL,
PRIMARY KEY(id),
FOREIGN KEY(user_id) REFERENCES users(id),
FOREIGN KEY(sensor_arrival_id) REFERENCES devices(id),
FOREIGN KEY(sensor_departure_id) REFERENCES devices(id)
