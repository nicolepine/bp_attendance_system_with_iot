﻿namespace BP_AttendanceSystem_Lepine.Models.DtoOut {
    public class ApiDtoOut {

        public enum Entry {
            Allow = 1,
            Blocked = -1
        }

        public class TokenDtoOut {
            public string? Token { get; set; }
        }

        public class Response {
            public Response(string message = "", int code = 200) {
                Message = message;
                Code = code;
            }
            public string? Message { get; set; }
            public int Code { get; set; }
        }
        public class UserInfo : Response {
            public UserInfo(string message = "", int code = 200): base(message, code) {
                
            }
            public string? FullName { get; set; }
        }
        public class Authorization : Response {
            public Authorization(string message = "", int code = 200) : base(message, code) {}
            public Entry AllowEntry { get; set; }
        }
    }
}
