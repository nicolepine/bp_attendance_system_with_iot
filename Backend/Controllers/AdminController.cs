﻿using BP_AttendanceSystem_Lepine.Helpers;
using BP_AttendanceSystem_Lepine.Models;
using BP_AttendanceSystem_Lepine.Models.Db;
using BP_AttendanceSystem_Lepine.Models.DtoIn;
using BP_AttendanceSystem_Lepine.Models.DtoOut;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using NuGet.Packaging.Signing;

namespace BP_AttendanceSystem_Lepine.Controllers {

    [Route("/Admin")]
    [AdminAuthorize]
    public class AdminController : Controller {
        private readonly ILogger<AdminController> _logger;
        private GlobalContext _globalContext;
        public AdminController(ILogger<AdminController> logger, GlobalContext gc) {
            _logger = logger;
            _globalContext = gc;
        }
        [HttpGet]
        public IActionResult Index() {
            return View();
        }
        #region Employees
        [HttpGet]
        [Route("Employee/Shifts/{id}")]
        public async Task<IActionResult> EmployeeShifts(int id = 0, DateTime from = default, DateTime to = default) {
            if (from == default || to == default) {
                from = DateTime.Now.AddMonths(-1);
                to = DateTime.Now;
            }
            IEnumerable<Shift> shifts = await _globalContext.Shifts
                .Include(s => s.User)
                .Include(s => s.ArrivalDevice)
                .Include(s => s.DepartureDevice)
                .Where(s => s.UserId == id
            && ((s.Arrival.Value.Date >= from.Date && s.Departure == null && s.Arrival.Value.Date <= to.Date)
            || (s.Arrival.Value.Date >= from.Date && s.Departure.Value.Date <= to.Date)
            || (s.Departure.Value.Date <= to.Date && s.Arrival == null && s.Departure.Value.Date >= from.Date)))
                .ToListAsync();
            ViewBag.From = from.Date;
            ViewBag.To = to.Date;
            return View(new PageUniversalModel<IEnumerable<Shift>>(shifts));

        }

        [HttpPost]
        [Route("Employee/Shifts/Update")]
        public async Task<IActionResult> EmployeeShiftUpdate(Shift shift) {
            if (shift == null) {
                return BadRequest(new ApiDtoOut.Response("Wrong format", 400));
            }
            if (await _globalContext.Shifts.AnyAsync(s => s.Id == shift.Id)) {
                _globalContext.Shifts.Update(shift);
                await _globalContext.SaveChangesAsync();
                return Ok(new ApiDtoOut.Response("Shift updated successfully"));
            }
            return BadRequest(new ApiDtoOut.Response("Shift does not exist", 400));
        }

            [HttpPost]
        [Route("Employee/Shifts/UpdateTime")]
        public async Task<IActionResult> EmployeeShiftUpdateTime(ShiftDtoIn.TimeUpdate shift) {
            if (!shift.Time.HasValue) {
                return BadRequest(new ApiDtoOut.Response("Wrong format", 400));
            }
            Shift shiftToUpdate = await _globalContext.Shifts.SingleOrDefaultAsync(s => s.Id == shift.Id);
            if (shiftToUpdate != default) {
                if(shift.Type == "arrival") {
                    if(shiftToUpdate.Arrival == null) {
                        shiftToUpdate.Arrival = shiftToUpdate.Departure;
                    }
                    shiftToUpdate.Arrival = shiftToUpdate.Arrival.Value.Date.Add(shift.Time.Value);
                    shiftToUpdate.Arrival = TimeZoneInfo.ConvertTimeToUtc(shiftToUpdate.Arrival.Value, TimeZoneInfo.Local);
                    shiftToUpdate.SensorArrivalId = null;
                } else {
                    if (shiftToUpdate.Departure == null) {
                        shiftToUpdate.Departure = shiftToUpdate.Arrival;
                    }
                    shiftToUpdate.Departure = shiftToUpdate.Departure.Value.Date.Add(shift.Time.Value);
                    shiftToUpdate.Departure = TimeZoneInfo.ConvertTimeToUtc(shiftToUpdate.Departure.Value, TimeZoneInfo.Local);
                    shiftToUpdate.SensorDepartureId = null;
                }
                _globalContext.Shifts.Update(shiftToUpdate);
                await _globalContext.SaveChangesAsync();
                return Ok(new ApiDtoOut.Response("Shift updated successfully"));
            }
            return BadRequest(new ApiDtoOut.Response("Shift does not exist", 400));
        }

        [HttpGet]
        [Route("Employee/List")]
        public async Task<IActionResult> EmployeeList() {
            IEnumerable<User> users = await _globalContext.Users
                .Include(u => u.Role)
                .ToListAsync();
            return View(new PageUniversalModel<IEnumerable<User>>(users));
        }

        [HttpGet]
        [Route("Employee/Edit/{id}")]
        [Route("Employee/Add")]
        public async Task<IActionResult> EmployeeAddEdit(int id = 0) {
            ViewBag.Roles = new SelectList(await _globalContext.Roles.ToListAsync(), "Id", "RoleName");
            User? user = new();
            if(id != 0) {
                user = await _globalContext.Users.SingleOrDefaultAsync(u => u.Id == id);
                if(user == default) {
                    user = null;
                }
            }
            return View(new PageUniversalModel<User>(user));
        }
        [HttpPost]
        [Route("Employee/Edit/{id}")]
        [Route("Employee/Add")]
        public async Task<IActionResult> EmployeeSave(PageUniversalModel<User> userDtoIn) {
            User user = userDtoIn.Data;
            ViewBag.Roles = new SelectList(await _globalContext.Roles.ToListAsync(), "Id", "RoleName");
            if (ModelState.IsValid) {
                bool passwordChanged = false;
                User? originalUser = await _globalContext.Users
                    .AsNoTracking()
                    .SingleOrDefaultAsync(u => u.Id == user.Id);
                if (user.Password != null) {
                    passwordChanged = true;
                    user.SetPassword(user.Password);
                }
                if (originalUser == default) {
                    await _globalContext.Users.AddAsync(user);
                } else {
                    if(!passwordChanged) {
                        user.PasswordHash = originalUser.PasswordHash;
                    }
                    _globalContext.Users.Update(user);
                }
                await _globalContext.SaveChangesAsync();
                return RedirectToAction("EmployeeList");
            }
            PageUniversalModel<User> model = new PageUniversalModel<User>(user);
            model.Message = model.GetStringFromValidation(ModelState.Values);
            return View("EmployeeAddEdit", model);
        }

        [HttpPost]
        [Route("Employee/Delete")]
        public async Task<IActionResult> EmployeeDelete(int id) {
            if(id == 0) {
                User? user = await _globalContext.Users
                    .Include(u => u.AccessDevices)
                    .Include(u => u.Shifts)
                    .SingleOrDefaultAsync(u => u.Id == id);
                if(user != default) {
                    user.Shifts.Clear();
                    user.AccessDevices.Clear();
                    _globalContext.Users.Update(user);
                    _globalContext.Users.Remove(user);
                    await _globalContext.SaveChangesAsync();
                    return Ok();
                }
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("Employee/Detail/{id}")]
        public async Task<IActionResult> EmployeeDetail(int id = 0) {
            User? u = await _globalContext.Users
                .Include(u => u.AccessDevices)
                .Include(u => u.Role)
                .SingleOrDefaultAsync(u => u.Id == id);
            if(u == default) {
                return RedirectToAction("EmployeeList");
            }
            ViewBag.AccessDevices = u.AccessDevices;
            return View(new PageUniversalModel<User>(u));
        }
        #endregion

        #region AccessDevices
        [HttpPost]
        [Route("AccessDevice/Save")]
        public async Task<IActionResult> SaveAccessDevice(AccessDevice accessDevice) {
            try {
                if (accessDevice == null) {
                    return BadRequest(new ApiDtoOut.Response("Chyba", 400));
                }
                if (accessDevice.Id == 0) {
                    await _globalContext.AccessDevices.AddAsync(accessDevice);
                } else {
                    _globalContext.AccessDevices.Update(accessDevice);
                }
                await _globalContext.SaveChangesAsync();
                _logger.LogInformation($"AccessDevice with id {accessDevice.Id} was updated/created");
                return Ok(new ApiDtoOut.Response("Uloženo"));
            } catch (Exception ex) {
                _logger.LogError(ex, "SaveDeviceError: ");
                return StatusCode(500);
            }
        }
        [HttpPost]
        [Route("AccessDevice/Remove")]
        public async Task<IActionResult> RemoveAccessDevice(int id) {
            AccessDevice? device = await _globalContext.AccessDevices.SingleOrDefaultAsync(ad => ad.Id == id);
            if (device == default) {
                return BadRequest();
            }
            _globalContext.AccessDevices.Remove(device);
            await _globalContext.SaveChangesAsync();
            return Ok();
        }
        #endregion

        #region Roles
        [HttpGet]
        [Route("Role/Manage")]
        public async Task<IActionResult> RoleManagement() {
            ViewBag.RolesToSkip = AppHelper.GetAdminIdRoleList();
            IEnumerable<Role> roles = await _globalContext.Roles.ToListAsync();
            return View(new PageUniversalModel<IEnumerable<Role>>(roles));
        }

        [HttpPost]
        [Route("Role/Save")]
        public async Task<IActionResult> RoleSave(Role role) {
            if (role != null) {
                if (await _globalContext.Roles.AnyAsync(r => r.Id == role.Id)) {
                    _globalContext.Roles.Update(role);
                } else {
                    await _globalContext.Roles.AddAsync(role);
                }
                await _globalContext.SaveChangesAsync();
                return Ok(new ApiDtoOut.Response("Role saved"));
            }
            return BadRequest(new ApiDtoOut.Response("Wrong format", 400));
        }

        [HttpPost]
        [Route("Role/Remove")]
        public async Task<IActionResult> RoleRemove(Role role) {
            if (role != null) {
                if (AppHelper.GetAdminIdRoleList().Contains(role.Id)) {
                    return BadRequest(new ApiDtoOut.Response("Cannot remove admin role", 400));
                }
                role = await _globalContext.Roles.Include(r => r.Users).SingleOrDefaultAsync(r => r.Id == role.Id);
                if (role == default) {
                    return BadRequest(new ApiDtoOut.Response("Role does not exist", 400));
                }
                if (role.Users.Any()) {
                    return BadRequest(new ApiDtoOut.Response("Cannot remove role which is assigned to employees", 400));
                }
                _globalContext.Roles.Remove(role);
                await _globalContext.SaveChangesAsync();
                return Ok(new ApiDtoOut.Response("Role removed successfully"));
            }
            return BadRequest(new ApiDtoOut.Response("Wrong format", 400));
        }

        #endregion

        #region Devices

        [HttpGet]
        [Route("Device/Manage")]
        public async Task<IActionResult> DeviceManagement() {
            IEnumerable<Device> devices = await _globalContext.Devices.ToListAsync();
            return View(new PageUniversalModel<IEnumerable<Device>>(devices));
        }

        [HttpPost]
        [Route("Device/Save")]
        public async Task<IActionResult> DeviceSave(Device device) {
            if (device != null) {
                if (await _globalContext.Devices.AnyAsync(d => d.Id == device.Id)) {
                    _globalContext.Devices.Update(device);
                } else {
                    await _globalContext.Devices.AddAsync(device);
                }
                await _globalContext.SaveChangesAsync();
                return Ok(new ApiDtoOut.Response("Device updated/added successfully"));
            }
            return BadRequest(new ApiDtoOut.Response("Wrong format", 400));
        }
        [HttpPost]
        [Route("Device/Remove")]
        public async Task<IActionResult> DeviceRemove(int id) {
            //Nemusí fungovat, pokud mají směny přiřazené následující zařízení
            try {
                Device device = await _globalContext.Devices.SingleOrDefaultAsync(d => d.Id == id);
                if (device != default) {
                    _globalContext.Devices.Remove(device);
                    await _globalContext.SaveChangesAsync();
                    return Ok(new ApiDtoOut.Response("Device removed successfully"));
                }
                return BadRequest(new ApiDtoOut.Response("Wrong format", 400));
            } catch {
                return StatusCode(500, new ApiDtoOut.Response("Device is assigned to some shifts, and cannot be removed", 500));
            }
        }
        #endregion
    }
}
