﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BP_AttendanceSystem_Lepine.Models.Db {
    [Table("access_devices")]
    public class AccessDevice {
        [Key]
        public int Id { get; set; }
        
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("chip_id")]
        public string? ChipId { get; set; }
        [Column("fingerprint_data")]
        public string? FingerprintData { get; set; }
        [Column("description")]
        [Display(Name = "Popis")]

        public string? Description { get; set; }
        [ForeignKey("UserId")]
        public virtual User? User { get; set; }

        public string GetAccessType() {
            if(ChipId != null) {
                return "Čip";
            } else {
                return "Otisk prstu";
            }
        }
    }
}
