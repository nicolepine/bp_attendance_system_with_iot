const smartcard = require('smartcard');

module.exports = function(RED) {
    function OnScanNode(config) {
        RED.nodes.createNode(this,config);
	const Devices = smartcard.Devices;
	const devices = new Devices();
devices.on('device-activated', (event => {
    //console.log(`Device '${event.device}' activated`);
    event.devices.map((device, index) => {
device.on('card-inserted', event => {
	let card = event.card;
        this.send({payload: {card: card}, state: "card-inserted"});

        card.on('command-issued', event => {
            console.log(`Command '${event.command}' issued to '${event.card}' `);
        });

        card.on('response-received', event => {
            console.log(`Response '${event.response}' received from '${event.card}' in response to '${event.command}'`);
        });
 });
device.on('card-removed', event => {
        this.send({payload: {card: event.card?.device}, state: "card-removed"})
    });
});
}));
    }
    RED.nodes.registerType("on-scan",OnScanNode);
}