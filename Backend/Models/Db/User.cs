﻿using BP_AttendanceSystem_Lepine.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BP_AttendanceSystem_Lepine.Models.Db {
    [Table("users")]
    public class User {
        public User() {
            Id = 0;
        }
        [Key]
        [Column("id")]
        public int? Id { get; set; }
        [Column("first_name")]
        [Display(Name = "Jméno")]
        [Required]
        public string? FirstName { get; set; }
        [Column("surname")]
        [Display(Name = "Příjmení")]
        [Required]
        public string? Surname { get; set; }
        [Column("email")]
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string? Email { get; set; }
        [Column("phone")]
        [Display(Name = "Telefon")]
        [Phone]
        public string? Phone { get; set; }
        [Column("password_hash")]
        [HiddenInput]
        public string? PasswordHash { get; set; }
        [Column("role_id")]
        [Display(Name = "Role")]
        [ForeignKey("Role")]
        public int? RoleId { get; set; }

        [Display(Name = "Role")]
        [ValidateNever]
        public virtual Role? Role { get; set; }
        [ValidateNever]

        public virtual ICollection<Shift>? Shifts { get; set; }

        [ValidateNever]
        public virtual ICollection<AccessDevice>? AccessDevices { get; set; }

        [NotMapped]
        [Display(Name = "Heslo")]
        public string? Password { get; set; }

        public void SetPassword(string password) {
            PasswordHash = AppHelper.ComputeSha256Hash(password);
        }

        public string GetFullName() {
            return $"{FirstName} {Surname}";
        }
    }
    
}
