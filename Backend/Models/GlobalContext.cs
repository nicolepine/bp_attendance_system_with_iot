﻿using BP_AttendanceSystem_Lepine.Models.Db;
using Microsoft.EntityFrameworkCore;

namespace BP_AttendanceSystem_Lepine.Models {
    public class GlobalContext : DbContext {
        public GlobalContext(DbContextOptions options) : base(options) {

        }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Shift> Shifts { get; set; } 
        public DbSet<AccessDevice> AccessDevices { get; set; }
    }
}
